import createApp from './app.js';




export default (context) => {
  let url = context.url
  return new Promise((resolve, reject) => {
    // 我们需要在服务端先把对应的页面先渲染出来
    const { app, router, store } = createApp(); // 创建了一个应用
    router.push(url); // 服务端跳转页面后, 在服务端会生成html字符串
  
    router.onReady(() => { // 表示组件已经准备完毕, 防止异步组件没有加载完成就渲染了
      const matchComponents = router.getMatchedComponents();
      if(!matchComponents.length) {
        // 服务端页面跳转后, 没有匹配到具体的组件
        return reject({ code: 404 })
      }
      Promise.all(matchComponents.map(component => {
        // Promise.all 为了让所有的组件的asyncData执行完毕
        // nuxtjs 里面就叫 asyncData
        if(component.asyncData) { // 此方法为了在服务端发请求
          return component.asyncData(store);
        }
      })).then(() => {
        context.state = store.state; // 相当于把服务端的状态放在了上下文中(就会在客户端window.__INITIAL_STATE__上挂载)
        resolve(app);
        // return app; // createRenderer.renderToString(vm)
      }, reject)
    }, reject)
  })
}
