const Vue = require('vue');
const VueServerRenderer = require('vue-server-renderer');
const fs = require('fs')
const path = require('path')
const static = require('koa-static')

const Koa = require('koa');
const Router = require('koa-router');
const app = new Koa();
const router = new Router();

let bundle = fs.readFileSync(path.resolve(__dirname, 'dist/server.bundle.js'), 'utf8');
let template = fs.readFileSync(path.resolve(__dirname, 'dist/index.ssr.html'), 'utf8');

const render = VueServerRenderer.createBundleRenderer(bundle, { // 使用服务端打包的结果
  template
})
// 访问静态文件时, 去dist目录找, 如果找不到则执行下面的
app.use(static(path.resolve(__dirname, 'dist')));

app.use(router.routes()); // 表示koa应用中加载了路由系统

// router.get('/', async (ctx) => {
//   // ctx.body = await render.renderToString();
//   ctx.body = await new Promise((resolve, reject) => {
//     render.renderToString((err, html) => {
//       // 渲染成字符串, 返回的是promise
//       /**注意: 
//        * 1. 使用 async await 不能解析样式, 只能通过回调的方式
//        * 2. js都在client.bundle.js上, 需要在html文件里面手动引入
//        */
//       resolve(html)
//     })
//   })
// })


// 当访问任意路径时都可以匹配
router.get('/(.*)', async (ctx) => {
  ctx.body = await new Promise((resolve, reject) => {
    render.renderToString({ url: ctx.url }, (err, html) => {
      if(err && err.code === 404) {
        return resolve('Not found!')
      }
      return resolve(html); // 当前路径访问不到直接返回首页, 前端路由, 会根据路径进行跳转
    })
  })
})

app.listen(3000);

// ssr 服务端渲染流程
