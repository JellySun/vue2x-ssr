const path = require('path');
const VueLoaderPlugin = require('vue-loader/lib/plugin'); // 规定只要用了 vue-loader, 就要用这个插件
const TerserPlugin = require("terser-webpack-plugin");

module.exports = {
  output: {
    filename: '[name].bundle.js',
    path: path.resolve(__dirname, '../dist')
  },
  optimization: {
    splitChunks: {
      minSize: 1024*1000
    },
    minimize: true,
    minimizer: [
      new TerserPlugin({
        // 不将注释提取到单独的文件中
        terserOptions: {
          format: {
            comments: false,
          },
        },
        extractComments: false,
      })
    ]
  },
  module: { // 对模块处理进行配置
    rules: [
      { test: /\.vue$/, use: { loader: 'vue-loader', options: { /* optimizeSSR: false */ }
      }},
      { test: /\.css$/, use: ['vue-style-loader', 'css-loader'] },
      { test: /\.js/, use: {
        loader: 'babel-loader',
        options: {
          presets: ['@babel/preset-env'] // 插件的集合预设, 把es6+ 转化成 es5
        }
      }}
    ]
  },
  plugins: [
    new VueLoaderPlugin()
  ]
}