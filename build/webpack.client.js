const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { merge } = require('webpack-merge');
const base = require('./webpack.config.js');
// vue-server-renderer/client-plugin
// vue-server-renderer/server-plugin 打包自动引入对应xx.bundle.js的, webpack5 不支持


module.exports = (env) => {
  return merge(base, {
    mode: env.NODE_ENV,
    entry: {
      client: path.resolve(__dirname, '../src/client-entry.js'),
    },
    plugins: [
      new HtmlWebpackPlugin({ // ssr打包时 客户端不需要 html
        filename: env.NODE_ENV === 'production'?'index2.html':'index.html',
        template: path.resolve(__dirname, '../public/index.html'),
        minify:{
          removeComments: true, // 清理html中的注释
          collapseWhitespace: false, // 不清理html中的空格、换行符
        }
      })
    ]
  })
}