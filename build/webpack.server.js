const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { merge } = require('webpack-merge');
// const nodeExternals = require('webpack-node-externals');
// const { webpack } = require('webpack');
// new webpack.ProvidePlugin({})
const base = require('./webpack.config.js');
module.exports = (env) => {
  return merge(base, {
    mode: env.NODE_ENV,
    entry: {
      server: path.resolve(__dirname, '../src/server-entry.js'),
    },
    output: {
      library: {
        type: 'commonjs2', // 把入口文件最终导出变成 module.exports = () => {}
      }
    },
    target: 'node', // 给 node 使用, 要用 commonJS 语法
    // Webpack 5，替换target: 'node' 为externalsPreset对象
    // externalsPresets: { node: true }, // 为了忽略诸如path、fs等内置模块
    // externals: [nodeExternals()], // 以忽略节点\模块文件夹中的所有模块
    plugins: [
      new HtmlWebpackPlugin({ // 默认会自动引入打好的包
        filename: 'index.ssr.html',
        template: path.resolve(__dirname, '../public/index.ssr.html'),
        excludeChunks: ['server'], // 忽略引入的js文件
        client: 'client.bundle.js', // 表示生成的html需要引入这个js: 客户端代码
        minify:{
          removeComments: false, // 不清理html中的注释
          collapseWhitespace: false, // 不清理html中的空格、换行符
        },
      })
    ]
  })
}