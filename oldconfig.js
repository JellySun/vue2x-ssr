const path = require('path')

// // 生成服务端的包
// const VueSSRServerPlugin = require("vue-server-renderer/server-plugin");
// // 生成客户端的包
// const VueSSRClientPlugin = require("vue-server-renderer/client-plugin");
// const nodeExternals = require("webpack-node-externals");
// const merge = require("lodash").merge;

// // 环境变量, 决定我们打包入口是客户端还是服务端
// const TARGET_NODE = process.env.WEBPACK_TARGET === "node";
// const target = TARGET_NODE ? "server" : "client";


const resolve = function (dir) {
  return path.join(__dirname, dir)
}

const isProduction = process.env.NODE_ENV === "production";

function proxy () {
  // localHost 可选值 'localhost'  ||  '127.0.0.1'  ||  '0.0.0.0'( 本机ip)
  const localHost = '0.0.0.0';
  const localPort = '8080';
  const context = ['/api', '/images'];
  // devServer 的代理
  let proxy = {};
  for (let val of context) {
    proxy[val] = {
      target: `http://172.16.16.8:8080`,
      changeOrigin: true,
      secure: false
    }
  }
  return { localHost, localPort, proxy: { ...proxy } }
}

module.exports = {
  publicPath: isProduction ? '/' : '/', // 根路经  './'相对路径
  // outputDir: './dist'+target,   // 构建输出目录
  outputDir: './dist',   // 构建输出目录
  assetsDir: 'assets', // 静态资源目录（js,css,img,fonts）
  lintOnSave: true,    // 是否开启eslint保存监测，有效值：true  ||  false  ||  'error'
  productionSourceMap: false, // 打包不生成 js.map 文件, 加快生产环境的打包速度，也能避免源码暴露在浏览器端
  devServer: {
    // index: 'home.html', // 指定页面入口, 多页面且首页不是index.html时需要
    host: proxy().localHost,
    port: proxy().localPort,
    open: true,
    https: false,
    hot: true, // 启动 HMR 热更新, 某些模块无法热更新时, 刷新页面
    hotOnly: false, // 某些模块无法热更新时, 不刷新页面, 控制台输出报错
    proxy: proxy().proxy
  },
  // 调整内部的 webpack 配置
  configureWebpack: config => {
    const newResolve = {
      extensions: ["css", ".js", ".vue", ".less", ".json"], //文件优先解析后缀名顺序
      alias: {
        "@": resolve("src"),
        "@assets": resolve("src/assets"),
        "@components": resolve("src/components"),
        "@config": resolve("src/config"),
        "@pages": resolve("src/pages"),
        "@utils": resolve("src/utils"),
      }
    }
    Object.assign(config, {
      resolve: newResolve,
    //   entry: `./src/entry-${target}.js`,
    //   devtool: 'source-map',
    //   target: TARGET_NODE ? "node" : "web",
    //   node: TARGET_NODE ? undefined : false,
    //   // 外置化应用程序依赖模块, 可以使用服务器构建速度更快, 并生成较小的 bundle 文件
    //   output: {
    //     // 此处告知 server bundle 使用 Node 风格导出模块
    //     libraryTarget: "commonjs2"
    //   },
    //   externals: TARGET_NODE
    //     ? nodeExternals({
    //         // 不要外置化 webpack 需要处理的依赖模块
    //         // 可以在这里添加更多的文件类型, 例如, 未处理 *.vue 原始文件
    //         // 你还应该将修改 `global` (例如: polyfill) 的依赖模块列入白名单
    //         allowlist: [/\.css$/]
    //       })
    //     : undefined,

    //   // 这是将服务器的整个输出构建为单个 JSON 文件的插件
    //   // 服务端默认文件名为 "vue-ssr-server-bundle.json"
    //   plugins: [TARGET_NODE ? new VueSSRServerPlugin() : new VueSSRClientPlugin()]
    })
  },
  // 修改 Loader 选项
  chainWebpack: config => {
    // config.module
    // .rule("vue")
    // .use("vue-loader")
    // .tap(options => {
    //   merge(options, {
    //     optmizeSSR: false
    //   })
    // })
  }
}