### 准备服务端渲染(SSR)
```js
1. yarn add vue-server-renderer express -D
`安装服务端渲染器以及nodejs服务器`
2. 新建 server 目录, 编写nodejs脚本
3. 安装 vue-router
4. 目录
 入口 app.js
 服务端入口 entry-server.js
 客户端入口 entry-client.js
 webpack 打包
5. 安装跨脚本插件 cross-env -D
6. 脚本配置 package.json
```

##### 安装依赖 (-D <=> --save-dev)
```dash
// node环境下直接编译vue实例
$ npm install vue vue-server-renderer koa koa-router koa-static
> vue-server-renderer: 把 vue 实例解析成字符串;
> koa: 启动一个 node 服务;
> koa-router: 浏览器访问路径时, 返回路径名字;
> koa-static: 指定访问静态文件所在的文件夹;
----------------
// 使用webpack编译ssr模式
$ npm install vue-loader vue-template-compiler vue-style-loader css-loader @babel/core babel-loader @babel/preset-env -D
> vue-loader: 解析 .vue 文件;
> vue-template-compiler: 识别(编译) .vue 文件中的 `template` 语法, (runtimeOnly);
> vue-style-loader: 支持ssr(style-loader和它功能一样,但不支持ssr), 把样式插入到 style 标签中;
> @babel/core: es6+转es5, 搭配 `@babel/preset-env` 将es6高级语法转化为低级语法;
> babel-loader: webpack 和 babel 的桥梁, (webpack通过babel-loader去找@babel/core, @babel/core内部去找@babel/preset-env);
-----------------
$ npm install webpack webpack-cli webpack-dev-server html-webpack-plugin concurrently -D
> webpack-cli: webpack解析命令行;
> html-webpack-plugin: 自动将打好的包插入到 html 文件;
> webpack-dev-server: 启动本地开发服务;
> concurrently: 可以同时执行多个命令(让客户端打包和服务端打包通过一条命令执行);
```